I. Ajout de disque
🌞 Ajouter un disque dur de 5Go à la VM backup.tp6.linux

NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0  6.3G  0 disk
sr0          11:0    1 1024M  0 rom
[alienor@backup ~]$



[alienor@backup ~]$ lsblk | grep disk
sda           8:0    0    8G  0 disk
sdb           8:16   0  6.3G  0 disk
[alienor@backup ~]$



II. Partitioning

🌞 Partitionner le disque à l'aide de LVM

[alienor@backup ~]$ sudo pvcreate /dev/sdb
[sudo] password for alienor:
  Physical volume "/dev/sdb" successfully created.
[alienor@backup ~]$




[alienor@backup ~]$ sudo pvs
sudo  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g     0
  /dev/sdb      lvm2 ---  <6.26g <6.26g
[alienor@backup ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rl
  PV Size               <7.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              1791
  Free PE               0
  Allocated PE          1791
  PV UUID               9xysSR-qmFm-SY3O-gCFW-YXAn-d0vB-rP9cdr

  "/dev/sdb" is a new physical volume of "<6.26 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               <6.26 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               n7YXvo-quKT-8ECo-7b4g-B4Z2-Idas-Ixx3KW

[alienor@backup ~]$


[alienor@backup ~]$ sudo vgcreate backup /dev/sdb
  Volume group "backup" successfully created
[alienor@backup ~]$





[alienor@backup ~]$ sudo vgs
su  VG     #PV #LV #SN Attr   VSize  VFree
  backup   1   0   0 wz--n-  6.25g 6.25g
  rl       1   2   0 wz--n- <7.00g    0
do[alienor@backup ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               backup
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0




[alienor@backup ~]$ sudo lvcreate -L 5G backup -n lv_name
  Logical volume "lv_name" created.
[alienor@backup ~]$ sudo lvs
lv  LV      VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lv_name backup -wi-a-----   5.00g                                                  
  root    rl     -wi-ao----  <6.20g                                                  
  swap    rl     -wi-ao---- 820.00m                                                  
[alienor@backup ~]$ lvdisplay
  WARNING: Running as a non-root user. Functionality may be unavailable.
  /run/lock/lvm/P_global:aux: open failed: Permission denied
[alienor@backup ~]$



[alienor@backup ~]$ sudo lvcreate -L 100%FREE backup -n malv
  Can't parse size argument.
  Invalid argument for --size: 100%FREE
  Error during parsing of command line.
[alienor@backup ~]$




🌞 Formater la partition


[alienor@backup ~]$ sudo mkfs -t ext4 /dev/backup/lv_name
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 1310720 4k blocks and 327680 inodes
Filesystem UUID: 9afe516b-bed5-4fa8-a3a4-8084e5fb3190
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done

[alienor@backup ~]$





🌞 Monter la partition



[alienor@backup ~]$ sudo mkdir /mnt/backup
mkdir: cannot create directory ‘/mnt/backup’: File exists
[alienor@backup ~]$ mkdir /dev/backup/lv_name /mnt/backup
mkdir: cannot create directory ‘/dev/backup/lv_name’: File exists
mkdir: cannot create directory ‘/mnt/backup’: File exists
[alienor@backup ~]$




[alienor@backup ~]$ mount
sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,relatime,seclabel)
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
devtmpfs on /dev type devtmpfs (rw,nosuid,seclabel,size=494556k,nr_inodes=123639,mode=755)
securityfs on /sys/kernel/security type securityfs (rw,nosuid,nodev,noexec,relatime)
tmpfs on /dev/shm type tmpfs (rw,nosuid,nodev,seclabel)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,seclabel,gid=5,mode=620,ptmxmode=000)
tmpfs on /run type tmpfs (rw,nosuid




[alienor@backup ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             483M     0  483M   0% /dev
tmpfs                502M     0  502M   0% /dev/shm
tmpfs                502M  6.8M  495M   2% /run
tmpfs                502M     0  502M   0% /sys/fs/cgroup
/dev/mapper/rl-root  6.2G  2.7G  3.6G  43% /
/dev/sda1           1014M  268M  747M  27% /boot
tmpfs                101M     0  101M   0% /run/user/1000
[alienor@backup ~]$

















Partie 2 : Setup du serveur NFS sur backup.tp6.linux


🌞 Préparer les dossiers à partager


[alienor@web ~]$ sudo mkdir /backup/web.tp6.linux/
mkdir: cannot create directory ‘/backup/web.tp6.linux/’: File exists
[alienor@web ~]$



[alienor@db ~]$ sudo mkdir /backup/db.tp6.linux
[alienor@db ~]$ sudo mkdir /backup/db.tp6.linux
mkdir: cannot create directory ‘/backup/db.tp6.linux’: File exists
[alienor@db ~]$







🌞 Install du serveur NFS

[alienor@backup ~]$ sudo dnf install nfs-utils
Rocky Linux 8 - AppStream   

alienor@backup ~]$ systemctl start nfs
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'nfs.service'.
Authenticating as: alienor
Password:
==== AUTHENTICATION COMPLETE ====
Failed to start nfs.service: Unit nfs.service not found.
[alienor@backup ~]$


[alienor@backup ~]$ sudo systemctl enable nfs-utils
The unit files have no installation config (WantedBy, RequiredBy, Also, Alias
settings in the [Install] section, and DefaultInstance for template units).
This means they are not meant to be enabled using systemctl.
Possible reasons for having this kind of units are:
1) A unit may be statically enabled by being symlinked from another unit's
   .wants/ or .requires/ directory.
2) A unit's purpose may be to act as a helper for some other unit which has
   a requirement dependency on it.
3) A unit may be started when needed via activation (socket, path, timer,
   D-Bus, udev, scripted systemctl call, ...).
4) In case of template units, the unit is meant to be enabled with some
   instance name specified.
[alienor@backup ~]$










🌞 Conf du serveur NFS


[alienor@backup etc]$ cat idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
#Domain = local.domain.edu


[alienor@backup etc]$ cat idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
#Domain = tp6.linux



[alienor@backup etc]$ cat exports

/backup/web.tp6.linux/ 10.1.5.11/24(rw,no_root_squash)
/backup/db.tp6.linux/ 10.1.5.12/24(rw,no_root_squash)
[alienor@backup etc]$




🌞 Démarrez le service

[alienor@backup etc]$ systemctl start nfs-server
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'nfs-server.service'.
Authenticating as: alienor
Password:
==== AUTHENTICATION COMPLETE ====
[alienor@backup etc]$ systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor preset: di>
   Active: active (exited) since Sat 2021-12-04 04:08:06 CET; 2min 5s ago
  Process: 24994 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl r>
  Process: 24982 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 24981 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
 Main PID: 24994 (code=exited, status=0/SUCCESS)

Dec 04 04:08:04 backup.tp6.linux systemd[1]: Starting NFS server and services...
Dec 04 04:08:06 backup.tp6.linux systemd[1]: Started NFS server and services.
lines 1-10/10 (END)




[alienor@backup etc]$ systemctl enable nfs-server
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: alienor
Password:
==== AUTHENTICATION COMPLETE ====
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: alienor
Password:
==== AUTHENTICATION COMPLETE ====
[alienor@backup etc]$





🌞 Firewall

[alienor@backup etc]$ sudo firewall-cmd --zone=public --add-port=2049/tcp
[sudo] password for alienor:
success
[alienor@backup etc]$


[alienor@backup ~]$ netstat -na | grep :2049
tcp        0      0 0.0.0.0:2049            0.0.0.0:*               LISTEN
tcp6       0      0 :::2049                 :::*                    LISTEN
[alienor@backup ~]$





[alienor@backup etc]$ ps -ef | grep nfs
root       24979       1  0 04:08 ?        00:00:00 /usr/sbin/nfsdcld
root       24986       2  0 04:08 ?        00:00:00 [nfsd]
root       24987       2  0 04:08 ?        00:00:00 [nfsd]
root       24988       2  0 04:08 ?        00:00:00 [nfsd]
root       24989       2  0 04:08 ?        00:00:00 [nfsd]
root       24990       2  0 04:08 ?        00:00:00 [nfsd]
root       24991       2  0 04:08 ?        00:00:00 [nfsd]
root       24992       2  0 04:08 ?        00:00:00 [nfsd]
root       24993       2  0 04:08 ?        00:00:00 [nfsd]
alienor    25078   24892  0 04:17 pts/0    00:00:00 grep --color=auto nfs
[alienor@backup etc]$








Partie 3 : Setup des clients NFS : et web.tp6.linuxdb.tp6.linux


Au début par .web.tp6.linux
🌞 Installer'


[alienor@web ~]$ sudo dnf install nfs-utils
[sudo] password for alienor:
Last metadata expiration check: 1 day, 6:32:42 ago on Thu 02 Dec 2021 09:49:44 PM CET.
Dependencies resolved.
================================================
 Package     Arch   Version        Repo    Size
================================================
Installing:
 nfs-utils   x86_64 1:2.3.3-46.el8 baseos 499 k
Installing dependencies:
 gssproxy    x86_64 0.8.0-19.el8   baseos 118 k
 keyutils    x86_64 1.5.10-9.el8   baseos  65 k
 libverto-libevent
             x86_64 0.3.0-5.el8    baseos  15 k
 rpcbind     x86_64 1.2.5-8.el8    baseos  69 k

Transaction Summary
================================================
Install  5 Packages

Total download size: 765 k
Installed size: 2.0 M
Is this ok [y/N]: y




🌞 Conf'


[alienor@web ~]$ cd /srv/
[alienor@web srv]$ mkdir backup
mkdir: cannot create directory ‘backup’: File exists
[alienor@web srv]$

[alienor@web etc]$ mkdir idmapd.conf
mkdir: cannot create directory ‘idmapd.conf’: File exists
[alienor@web etc]$



[alienor@web etc]$ sudo nano idmapd.conf
[alienor@web etc]$ cat idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
#Domain = tp6.linux

# In multi-domain envi




[alienor@web ~]$ sudo mount -t nfs 10.5.1.13:/backup/web.tp6.linux/ /srv/backup
mount.nfs: access denied by server while mounting 10.5.1.13:/backup/web.tp6.linux/
[alienor@web ~]$





🌞 Montage!


[alienor@web ~]$ sudo mount /backup/web.tp6.linux/ /srv/backup
mount: /srv/backup: /backup/web.tp6.linux is not a block device.
[alienor@web ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             483M     0  483M   0% /dev
tmpfs                502M     0  502M   0% /dev/shm
tmpfs                502M  6.8M  495M   2% /run
tmpfs                502M     0  502M   0% /sys/fs/cgroup
/dev/mapper/rl-root  6.2G  3.0G  3.2G  49% /
/dev/sda1           1014M  268M  747M  27% /boot
tmpfs                101M     0  101M   0% /run/user/1000
[alienor@web ~]$





Last login: Sat Dec  4 05:44:12 2021 from 10.5.1.5
[alienor@backup ~]$ vim /etc/fstab /backup/web.tp6.linux/ /srv/backup ext4 defaults 0 0



[alienor@web ~]$ sudo umount /backup/web.tp6.linux
[sudo] password for alienor:
umount: /backup/web.tp6.linux: not mounted.
[alienor@web ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
[alienor@web ~]$ sudo reboot
Connection to 10.5.1.11 closed by remote host.
Connection to 10.5.1.11 closed.





🌞 Répétez les opérations sur db.tp6.linux





[alienor@db ~]$ sudo dnf install nfs-utils
[sudo] password for alienor:
Last metadata expiration check: 1 day, 7:37:08 ago on Thu 02 Dec 2021 10:10:39 PM CET.
Dependencies resolved.
===========================================================================================
 Package                    Architecture    Version                  Repository       Size
===========================================================================================
Installing:
 nfs-utils                  x86_64          1:2.3.3-46.el8           baseos          499 k
Installing dependencies:
 gssproxy                   x86_64          0.8.0-19.el8             baseos          118 k
 keyutils                   x86_64          1.5.10-9.el8             baseos           65 k
 libverto-libevent          x86_64          0.3.0-5.el8              baseos           15 k
 rpcbind                    x86_



[alienor@db srv]$ ls /backup
db.tp6.linux
[alienor@db srv]$ cat /backup
cat: /backup: Is a directory
[alienor@db srv]$ sudo mkdir /srv/
mkdir: cannot create directory ‘/srv/’: File exists
[alienor@db srv]$




[alienor@db ~]$ cd /etc/
[alienor@db etc]$ sudo nano idmapd.conf
[alienor@db etc]$ cat idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
#Domain = tp6.linux


[alienor@db etc]$ sudo mount -t nfs 10.5.1.13:/backup/db.tp6.linux/ /srv/backup
mount.nfs: access denied by server while mounting 10.5.1.13:/backup/web.tp6.linux/
[alienor@db etc]$



[alienor@db etc]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             483M     0  483M   0% /dev
tmpfs                502M     0  502M   0% /dev/shm
tmpfs                502M  6.8M  495M   2% /run
tmpfs                502M     0  502M   0% /sys/fs/cgroup
/dev/mapper/rl-root  6.2G  3.0G  3.3G  49% /
/dev/sda1           1014M  268M  747M  27% /boot
tmpfs                101M     0  101M   0% /run/user/1000
[alienor@db etc]$
























Partie 4 : Scripts de sauvegarde
I. Sauvegarde Web
🌞 Ecrire un script qui sauvegarde les données de NextCloud






🌞 Créer un service


🌞 Vérifier que vous êtes capables de restaurer les données


🌞 Créer un timer





II. Sauvegarde base de données
🌞 Ecrire un script qui sauvegarde les données de la base de données MariaDB






🌞 Créer un service



🌞 Créer un timer







