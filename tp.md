
# Tp1, objectif : plusieurs manière de faire crasher son pc
---
## premiere manière 
* ':(){ :|: & };:'

goal: elle se replique à l'infini, elle mange toute la mémoire de l'ordinateur. 


## deuximè manière 
* 'sudo dd if=/dev/random of=/dev/sda'

goal: elle supprime toute les données 

## troisième manière 
* 'sudo rm -rf /* '


goal:  elle supprime toute les répertoires 

## quatrième manière 
*'xinput list'
* 'xinput float 11'

goal: desactive le clavier, on utiliser xinput list puis regarde a la ligne AT Translated Set 2 keyboard et on prend id qui est 11, puis xinput float 11 pour desactiver le clavier. 


## cinquième manière 
* 'sudo dd if=/dev/random of=/dev/port'

goal: crée la panique du noyau, la machine se ferme directement

## sixième manière 
* 'sudo dd if=/dev/zero of=/dev/sda'

goal: efface le contenue du disque en entier. 



